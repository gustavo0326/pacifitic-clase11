import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
//Módulo para habilitar el ngModel en inputs
import { FormsModule } from '@angular/forms';
//Enrutador para paginación
import { RouterModule, Routes } from '@angular/router';
//Módulo para habilitar las peticiones http
import { HttpModule } from '@angular/http';
//Módulo de cliente para usar httl
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LazyComponent } from './lazy/lazy.component';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { CuadernoComponent } from './cuaderno/cuaderno.component';
import { HomeComponent } from './home/home.component';
import { MatchesComponent } from './matches/matches.component';
import { PlayersComponent } from './players/players.component';
import { GroupsComponent } from './groups/groups.component';
import { TeamsComponent } from './teams/teams.component';
import { PlayerDetailComponent } from './player-detail/player-detail.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { PlayerService, MatchService } from './providers/providers';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NullAttrPipe } from './pipes/null-attr.pipe';
import { MusicMatchComponent } from './music-match/music-match.component';
import { ArtistsComponent } from './artists/artists.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';

export const rutasApp = [
  {
    path: 'home', component: HomeComponent, name: 'Home'
  },
  {
    path: 'matches', component: MatchesComponent, name: 'Matches'
  },
  {
    path: 'players',  name: 'Players',
    children: [
      {
        path: '',
        component: PlayersComponent
      },
      {
        path: ':id',
        component: PlayerDetailComponent
      }
    ]
  },
  {
    path: 'groups', component: GroupsComponent, name: 'Groups'
  },
  {
    path: 'teams', name: 'Teams',
    children:[
      {
        path: '',
        component: TeamsComponent
      },
      {//todos los elementos en el path con : por delante son variables
        path: ':id',
        component: TeamDetailComponent
      }
    ]
  },
  {
    path: 'musicMatch', name: 'MusicMatch',
    children: [
      {
        path: '',
        component: MusicMatchComponent
      },
      {
        path: 'artists',
        children: [
          {
            path: '',
          component: ArtistDetailComponent
        },
          {
            path: ':query',
          component: ArtistDetailComponent
        }
        ]
      },
      {
        path: ':id',
        component: ArtistDetailComponent
      }
    ]
  },
  {
    path: 'lazy', component: LazyComponent, name: 'Lazy'
  },
  {
    path: 'calculadora', component: CalculadoraComponent, name: 'Calculadora'
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  //{ path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LazyComponent,
    CalculadoraComponent,
    CuadernoComponent,
    HomeComponent,
    MatchesComponent,
    PlayersComponent,
    GroupsComponent,
    TeamsComponent,
    PlayerDetailComponent,
    TeamDetailComponent,
    NullAttrPipe,
    MusicMatchComponent,
    ArtistsComponent,
    ArtistDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(
      rutasApp,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [
    PlayerService,
    MatchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

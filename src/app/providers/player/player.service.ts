import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API, headers } from '../providers';

interface Player {
  id: Number,
  name: String,
  synonyms: String,
  code: Number,
  born_at: String,
  city_id: Number,
  region_id: Number,
  country_id: Number,
  nationality_id: Number
}

@Injectable({
  providedIn: 'root'
})

export class PlayerService {

  private lastRequest: any;
  private api = API;
  private service = "persons/";
  public player: Player = {
    id: null,
    name: null,
    synonyms: null,
    code: null,
    born_at: null,
    city_id: null,
    region_id: null,
    country_id: null,
    nationality_id: null

  }

  constructor(private http: HttpClient) { }

  get(id?) {
    var d = new Date(this.lastRequest);
    console.log(d);
    
    let uri = `${this.api}${this.service}`;
    if(id){
      if (this.player.id == null) {
        this.getById(uri,id);
      }else{
        //si la última petición es menor a 1 minuto, entonces  no actualice
        if ( (Date.now() - this.lastRequest) > 60000){
          this.getById(uri, id);
        }else{
          console.log("Aún no ha pasado 1 minuto desde la última consulta");
        }
      }
    }else{
      var d = new Date(this.lastRequest);
      console.log(`Request de todos, at ${d}`)
      return this.http.get(uri, { headers: headers });
    }
  }

  getById(uri,id){
    this.http.get(uri, { headers: headers }).subscribe((players) => {
      let player;
      for (let i = 0; i < Object.keys(players).length; i++) {
        if (players[i].id == id) {
          player = players[i];
          break;
        }
      }
      this.player = player;
      this.lastRequest = Date.now();
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerService } from '../providers/providers';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.scss']
})
export class PlayerDetailComponent implements OnInit {
  
  private id: any;

  constructor(public route: ActivatedRoute, public playerService: PlayerService) { 
    //para adicionar más parámetros, los podemos separar en la url con ;
    //http://localhost:4200/players/4;foo=bar
    this.route.params.subscribe(params =>{ console.log(params); this.id = params.id;});
  }

  ngOnInit() {
    this.playerService.get(this.id);
  }

}

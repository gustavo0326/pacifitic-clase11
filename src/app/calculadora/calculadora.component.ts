import { Component, OnInit, HostListener, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.scss']
})
export class CalculadoraComponent implements OnInit {

  @Output() escribir = new EventEmitter<string>();

  @HostListener('window:keydown',['$event'])
  keywordInput(event:KeyboardEvent){
    console.log(event.type);
    console.log(`${event.key} is NaN? = ${isNaN(parseFloat(event.key))}, let's parse it: ${parseFloat(event.key)}`);
    if(!isNaN(parseFloat(event.key))){
      this.adicionarAMemoria(event.key);
    }else{
      if(event.key == "Enter"){
        this.operar();
      }else{
        this.establecerOperador(event);
      }
    }
  }

  public text: string = "Hola";
  operador: any;
  valor: number = 0;
  display: any = 0;
  private a: any; 
  private b: any; 
  private memoria: string
  operationMemory: any ="";

  constructor() { 
    this.memoria = "";
  }

  ngOnInit() {
  }

  pressBtn(val){
    let btns = document.querySelectorAll(".btn");
    for (let i = 0; i < btns.length; i++) {
      if(btns[i].innerHTML == val){
        btns[i].classList.add("pressedBtn");
        setTimeout(() => btns[i].classList.remove("pressedBtn"), 300)
      }
    }
  }

  adicionarAMemoria(val){
    this.memoria+=val;
    console.log(this.memoria);
    this.display = this.memoria;
    this.addToOperationMemory(val);
    this.pressBtn(val);
  }

  addToOperationMemory(val){
    this.operationMemory+=val;
    this.escribir.emit(this.operationMemory);
  }

  establecerOperador(e){
    if (e.type == "keydown"){
      this.operador = e.key;
    }else{
      this.operador = e.target.innerHTML;
    }
    this.pressBtn(this.operador);
    this.a = this.display;
    this.display = 0;
    this.memoria = "";
    this.addToOperationMemory(this.operador);
  }

  operar(){
    switch (this.operador) {
      case "+":
          this.display = parseFloat(this.a) + parseFloat(this.memoria);
          this.text = "chao";
        break;
    
      default:
        break;
    }
    this.pressBtn("=");
  }
  test(){
    console.log(this.text);
  }
}
